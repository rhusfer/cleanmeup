package com.hackathon.cleanmeup

import android.app.Application
import android.content.Context
import com.google.firebase.FirebaseApp

/**
 * Created on 2019-05-05 - 13:02
 * @author Rhusfer John C. Cuezon
 */
class App : Application() {
    override fun onCreate() {
        super.onCreate()

        FirebaseApp.initializeApp(this)
    }
}