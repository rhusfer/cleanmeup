package com.hackathon.cleanmeup.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Resources
import android.location.Criteria
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.hackathon.cleanmeup.R
import com.hackathon.cleanmeup.models.DirtyArea
import kotlinx.android.synthetic.main.activity_location_select.view.*

/**
 * Created on 2019-05-05 - 10:50
 * @author Rhusfer John C. Cuezon
 */
class MapGetLocationFragment : DialogFragment(), OnMapReadyCallback {

    private var mMap: MapView? = null
    private var googleMap: GoogleMap? = null
    private lateinit var mLocationManager: LocationManager

    companion object {
        private const val MY_LOCATION_REQUEST_CODE = 1229
    }

    private var listener: ((loc: LatLng) -> Unit)? = null
    fun setOnLocationSelect(listener: (loc: LatLng) -> Unit) {
        this.listener = listener
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        mMap?.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStyle(STYLE_NORMAL, android.R.style.Theme_Material_NoActionBar_Fullscreen)

        mLocationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.activity_location_select, container, false)

        mMap = view.mapView
        mMap?.onCreate(savedInstanceState)
        mMap?.getMapAsync(this)

        view.buttonSelect.setOnClickListener {
            googleMap?.cameraPosition?.target?.run {
                listener?.invoke(this)
                dismiss()
            }
        }

        view.buttonCancel.setOnClickListener {
            dismiss()
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        mMap?.onResume()
    }

    override fun onPause() {
        super.onPause()
        mMap?.onPause()
    }

    override fun onStart() {
        super.onStart()
        mMap?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mMap?.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMap?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMap?.onLowMemory()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == MY_LOCATION_REQUEST_CODE) {
            if (permissions.size == 1 &&
                permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                initializeLocation()
            } else {
                // TODO show popup here
                // Permission was denied. Display an error message.
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap

        if (ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            this.googleMap?.isMyLocationEnabled = true
            initializeLocation()
        } else {
            requestLocationPermission()
        }
    }

    @SuppressLint("MissingPermission")
    private fun initializeLocation() {
        val provider = mLocationManager.getBestProvider(Criteria(), false)
        val location = mLocationManager.getLastKnownLocation(provider)

        val currentLocation = LatLng(location.latitude, location.longitude)
        this.googleMap?.isMyLocationEnabled = true
        this.googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 12f))
        this.googleMap?.setOnMarkerClickListener {
            AreaDetailsFragment.getInstance().show(childFragmentManager)
            true
        }

        this.googleMap?.setOnCameraMoveListener {
            this.googleMap?.cameraPosition?.target?.run {
                googleMap?.clear()
                googleMap?.addMarker(MarkerOptions().position(this))
            }
        }
    }

    private fun requestLocationPermission() {
        requestPermissions(
            arrayOf(Manifest.permission.READ_CONTACTS),
            MY_LOCATION_REQUEST_CODE
        )
    }


    fun show(fragmentManager: FragmentManager) {
        val transaction = fragmentManager.beginTransaction()
        val prev = fragmentManager.findFragmentByTag("location_select")

        if (prev != null) {
            val df = prev as AreaDetailsFragment
            df.dismiss()
            transaction.remove(prev)
            transaction.commit()
        }

        transaction.addToBackStack(null)
        show(transaction, "location_select")
    }
}