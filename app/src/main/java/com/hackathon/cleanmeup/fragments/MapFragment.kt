package com.hackathon.cleanmeup.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.ProgressDialog.show
import android.content.Context
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat.requestPermissions
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.hackathon.cleanmeup.R
import com.hackathon.cleanmeup.models.DirtyArea
import kotlinx.android.synthetic.main.activity_maps.view.*

/**
 * Created on 2019-05-05 - 10:50
 * @author Rhusfer John C. Cuezon
 */
class MapFragment : Fragment(), OnMapReadyCallback {

    private var list: List<DirtyArea>? = null
    private var mMap: MapView? = null
    private var googleMap: GoogleMap? = null
    private lateinit var mLocationManager: LocationManager

    companion object {
        private const val MY_LOCATION_REQUEST_CODE = 1229
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        mMap?.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mLocationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.activity_maps, container, false)

        mMap = view.mapView
        mMap?.onCreate(savedInstanceState)
        mMap?.getMapAsync(this)

        return view
    }

    override fun onResume() {
        super.onResume()
        mMap?.onResume()
    }

    override fun onPause() {
        super.onPause()
        mMap?.onPause()
    }

    override fun onStart() {
        super.onStart()
        mMap?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mMap?.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMap?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMap?.onLowMemory()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == MY_LOCATION_REQUEST_CODE) {
            if (permissions.size == 1 &&
                permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                initializeLocation()
            } else {
                // TODO show popup here
                // Permission was denied. Display an error message.
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap


        if (ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            this.googleMap?.isMyLocationEnabled = true
            initializeLocation()
        } else {
            requestLocationPermission()
        }
    }

    @SuppressLint("MissingPermission")
    private fun initializeLocation() {
        val provider = mLocationManager.getBestProvider(Criteria(), false)
        val location = mLocationManager.getLastKnownLocation(provider)

        val currentLocation = LatLng(location.latitude, location.longitude)
        this.googleMap?.isMyLocationEnabled = true
        this.googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 14f))
        this.googleMap?.setOnMarkerClickListener { marker ->
            list?.find { it.latlng == marker.position }?.run {
                val fragment = AreaDetailsFragment.getInstance()
                fragment.setPlace(this@run)
                fragment.show(childFragmentManager)
            }
            true
        }

        loadList()
    }

    private fun loadList() {
        FirebaseFirestore.getInstance()
            .collection("places")
            .addSnapshotListener { querySnapshot, _ ->
                list = querySnapshot?.documents?.map {
                    val geoPoint = it.get("location", GeoPoint::class.java)
                    DirtyArea(
                        it.get("name", String::class.java) ?: "",
                        LatLng(geoPoint?.latitude ?: 0.0, geoPoint?.longitude ?: 0.0),
                        it.get("rating", Int::class.java) ?: 0,
                        it.get("description", String::class.java) ?: ""
                    ).apply {
                        areaType = it.get("areaType", String::class.java) ?: ""
                        wasteType = it.get("wasteType", String::class.java) ?: ""
                        area = it.get("area", Int::class.java) ?: 0
                    }
                }

                this.googleMap?.clear()

                list?.forEach {
                    val strokeColor: Int
                    val fillColor: Int

                    when {
                        it.rating < 4 -> {
                            strokeColor = ContextCompat.getColor(context!!, R.color.stroke_mild)
                            fillColor = ContextCompat.getColor(context!!, R.color.fill_mild)
                        }
                        it.rating < 7 -> {
                            strokeColor = ContextCompat.getColor(context!!, R.color.stroke_moderate)
                            fillColor = ContextCompat.getColor(context!!, R.color.fill_moderate)
                        }
                        else -> {
                            strokeColor = ContextCompat.getColor(context!!, R.color.stroke_severe)
                            fillColor = ContextCompat.getColor(context!!, R.color.fill_severe)
                        }
                    }

                    val circle = CircleOptions()
                        .center(it.latlng)
                        .radius(it.area.toDouble())
                        .strokeColor(strokeColor)
                        .fillColor(fillColor)

                    this.googleMap?.addCircle(circle)

                    this.googleMap?.addMarker(
                        MarkerOptions()
                            .position(it.latlng)
                    )
                }
            }

    }

//    private fun updateMarkers() {
//        this.googleMap?.clear()
//        tempLocations.forEach {
//            this.googleMap?.addMarker(MarkerOptions().position(it.latlng))
//        }
//    }
//
//    private val tempLocations = listOf(
//        DirtyArea("Vano Beach", LatLng(10.258464, 123.982005), 5, "all of waste"),
//        DirtyArea("Pasil Fish Port", LatLng(10.289537, 123.892069), 5, "all of waste"),
//        DirtyArea("Ermita Cebu", LatLng(10.291073, 123.899257), 5, "all of waste"),
//        DirtyArea("Tinago, Cebu City", LatLng(10.297291, 123.910158), 5, "all of waste"),
//        DirtyArea("Cebu City", LatLng(10.302780, 123.915522), 5, "all of waste"),
//        DirtyArea("Madaue City", LatLng(10.319711, 123.944748), 5, "all of waste")
//    )

    private fun requestLocationPermission() {
        requestPermissions(
            arrayOf(Manifest.permission.READ_CONTACTS),
            MY_LOCATION_REQUEST_CODE
        )
    }
}