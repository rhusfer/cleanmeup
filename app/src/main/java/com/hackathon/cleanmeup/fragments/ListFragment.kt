package com.hackathon.cleanmeup.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.hackathon.cleanmeup.R
import com.hackathon.cleanmeup.adapters.AreaListAdapter
import com.hackathon.cleanmeup.models.DirtyArea
import kotlinx.android.synthetic.main.fragment_area_list.view.*

/**
 * Created on 2019-05-05 - 11:22
 * @author Rhusfer John C. Cuezon
 */
class ListFragment : Fragment() {

    private lateinit var adapter: AreaListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_area_list, container, false)

        adapter = AreaListAdapter(mutableListOf())

        adapter.setOnItemTap { place ->
            val fragment = AreaDetailsFragment.getInstance()
            fragment.setPlace(place)
            fragment.show(childFragmentManager)
        }


        view.recycleViewList.adapter = adapter
        view.recycleViewList.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadList()
    }

    private fun loadList() {
        FirebaseFirestore.getInstance()
            .collection("places")
            .addSnapshotListener { querySnapshot, _ ->
                val list = querySnapshot?.documents?.map {
                    Log.d("Rhusfer", "test : ${it.get("name", String::class.java)}")
                    val geoPoint =  it.get("location", GeoPoint::class.java)
                    DirtyArea(
                        it.get("name", String::class.java) ?: "",
                        LatLng(geoPoint?.latitude ?: 0.0, geoPoint?.longitude ?: 0.0),
                        it.get("rating", Int::class.java) ?: 0,
                        it.get("description", String::class.java) ?: ""
                    ).apply {
                        areaType = it.get("areaType", String::class.java) ?: ""
                        wasteType = it.get("wasteType", String::class.java) ?: ""
                        area = it.get("area", Int::class.java) ?: 0
                    }
                }?.sortedByDescending { it.rating } ?: listOf()

                adapter.reloadList(list)
            }

    }
}