package com.hackathon.cleanmeup.fragments

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hackathon.cleanmeup.R
import com.hackathon.cleanmeup.models.DirtyArea
import kotlinx.android.synthetic.main.dialog_fragment_area.view.*

/**
 * Created on 2019-05-04 - 17:39
 * @author Rhusfer John C. Cuezon
 */
class AreaDetailsFragment : DialogFragment() {
    private lateinit var place: DirtyArea
    private val dialogTag: String = "area_fragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStyle(STYLE_NORMAL, R.style.Theme_AppCompat_Light_Dialog_Alert)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_fragment_area, container, false)

        view.buttonClose.setOnClickListener {
            dismiss()
        }

        view.seekBarRating.progress = place.rating

        // Disable seekBar touch
        view.seekBarRating.setOnTouchListener { _, _ ->
            true
        }

        view.textViewTitle.text = place.name
        view.seekBarRating.progress = place.rating
        view.textViewAreaType.text = place.areaType
        view.textViewWasteType.text = place.wasteType
        view.textViewAreaSize.text = "${place.area} sq.m"
        view.textViewComment.text = place.description

        return view
    }

    fun show(fragmentManager: FragmentManager) {
        val transaction = fragmentManager.beginTransaction()
        val prev = fragmentManager.findFragmentByTag(dialogTag)

        if (prev != null){
            val df =  prev as AreaDetailsFragment
            df.dismiss()
            transaction.remove(prev)
            transaction.commit()
        }

        transaction.addToBackStack(null)
        show(transaction, dialogTag)
    }

    fun setPlace(place: DirtyArea) {
        this.place = place
    }

    companion object {
        fun getInstance() = AreaDetailsFragment()
    }
}