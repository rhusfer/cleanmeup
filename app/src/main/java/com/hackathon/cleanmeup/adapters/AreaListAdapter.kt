package com.hackathon.cleanmeup.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hackathon.cleanmeup.R
import com.hackathon.cleanmeup.models.DirtyArea
import kotlinx.android.synthetic.main.row_area.view.*

/**
 * Created on 2019-05-05 - 11:24
 * @author Rhusfer John C. Cuezon
 */
class AreaListAdapter(private var list: MutableList<DirtyArea>) : RecyclerView.Adapter<AreaListAdapter.AreaViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AreaViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.row_area, p0, false)
        return AreaViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: AreaViewHolder, position: Int) {
        val place = list[position]
        holder.setView(place)
        holder.itemView.setOnClickListener {
            callback?.invoke(place)
        }
    }

    class AreaViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun setView(area: DirtyArea) {
            itemView.textViewTitle.text = area.name
            itemView.textViewArea.text = "${area.area} sq.m"
            itemView.textViewAreaType.text = area.areaType
            itemView.textViewWasteType.text = area.wasteType

            val rating = when {
                area.rating < 4 -> "Mild"
                area.rating < 7 -> "Moderate"
                else -> "Severe"
            }

            itemView.textViewRating.text = rating
        }
    }

    fun reloadList(list: List<DirtyArea>) {
        this.list.clear()
        this.list.addAll(list)

        notifyDataSetChanged()
    }

    private var callback: ((place: DirtyArea) -> Unit)? = null
    fun setOnItemTap(callback: (place: DirtyArea) -> Unit) {
        this.callback = callback
    }
}