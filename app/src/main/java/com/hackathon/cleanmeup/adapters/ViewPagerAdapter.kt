package com.hackathon.cleanmeup.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.hackathon.cleanmeup.fragments.ListFragment
import com.hackathon.cleanmeup.fragments.MapFragment

/**
 * Created on 2019-05-05 - 12:09
 * @author Rhusfer John C. Cuezon
 */
class ViewPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
    private val mapFragment = MapFragment()
    private val listFragment = ListFragment()

    override fun getItem(p0: Int): Fragment {
        return if (p0 == 0) {
            mapFragment
        } else {
            listFragment
        }
    }

    override fun getCount(): Int = 2

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            "Map"
        } else {
            "List"
        }
    }
}