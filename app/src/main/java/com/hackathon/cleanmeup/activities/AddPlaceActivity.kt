package com.hackathon.cleanmeup.activities

import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_add.*
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.hackathon.cleanmeup.fragments.MapGetLocationFragment


/**
 * Created on 2019-05-05 - 15:19
 * @author Rhusfer John C. Cuezon
 */
class AddPlaceActivity : AppCompatActivity() {
    private var location: LatLng? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.hackathon.cleanmeup.R.layout.activity_add)

        setSupportActionBar(appToolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Add Place for cleaning target"

        buttonAdd.setOnClickListener {
            location?.run {
                addPlace(this)
            }
        }

        buttonAddLocation.setOnClickListener {
            val dialog = MapGetLocationFragment()
            dialog.setOnLocationSelect {
                location = it
            }
            dialog.show(supportFragmentManager)
        }
    }

    private fun addPlace(location: LatLng) {
        val data = HashMap<String, Any>()

        data["name"] = editTextPlace.text.toString()
        data["rating"] = seekBarRating.progress
        data["description"] = editTextDescription.text.toString()
        data["area"] = editTextArea.text.toString().toInt()
        data["areaType"] = editTextAreaType.text.toString()
        data["wasteType"] = editTextWasteType.text.toString()
        data["location"] = GeoPoint(location.latitude, location.longitude)
        data["createdBy"] = FirebaseAuth.getInstance().currentUser?.uid ?: "Unknown"

        FirebaseFirestore.getInstance()
            .collection("places")
            .add(data)
            .addOnSuccessListener {
                Snackbar.make(buttonAdd, "Place added.", Snackbar.LENGTH_SHORT).show()

                Handler().postDelayed({
                    finish()
                }, 1000)
            }
            .addOnFailureListener { e ->
                Snackbar.make(buttonAdd, "Place failed to be added.", Snackbar.LENGTH_SHORT).show()
                e.printStackTrace()
            }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            this.finish()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }
}