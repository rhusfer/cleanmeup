package com.hackathon.cleanmeup.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.google.android.gms.maps.GoogleMap
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat.startActivity
import android.view.Menu
import android.view.MenuItem
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.hackathon.cleanmeup.R
import com.hackathon.cleanmeup.adapters.ViewPagerAdapter
import com.hackathon.cleanmeup.fragments.ListFragment
import com.hackathon.cleanmeup.fragments.MapFragment
import com.hackathon.cleanmeup.models.DirtyArea
import kotlinx.android.synthetic.main.activity_main.*


class MapsActivity : AppCompatActivity() {

    private lateinit var mapFragment: MapFragment
    private lateinit var listFragment: ListFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mapFragment = MapFragment()
        listFragment = ListFragment()

        tabLayout.setupWithViewPager(viewPager)
        viewPager.adapter = ViewPagerAdapter(supportFragmentManager)

        setSupportActionBar(appToolbar)

        fabAddLocation.setOnClickListener {
            startActivity(Intent(this, AddPlaceActivity::class.java))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == R.id.action_logout) {
            logout()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    private fun logout() {
        FirebaseAuth.getInstance().signOut()
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }
}
