package com.hackathon.cleanmeup.activities

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.hackathon.cleanmeup.R

/**
 * Created on 2019-05-05 - 12:56
 * @author Rhusfer John C. Cuezon
 */
class SplashActivity : AppCompatActivity() {
    private lateinit var countDownTimer: CountDownTimer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        countDownTimer = object : CountDownTimer(3000, 1000) {
            override fun onFinish() {
                goToNextPage()
            }

            override fun onTick(p0: Long) {
                // Do nothing
            }

        }
    }

    override fun onResume() {
        super.onResume()

        countDownTimer.start()
    }

    override fun onPause() {
        countDownTimer.cancel()
        super.onPause()
    }

    private fun goToNextPage() {
        val auth = FirebaseAuth.getInstance()
        val intent = if (auth.currentUser == null) {
            Intent(this, LoginActivity::class.java)
        } else {
            Intent(this, MapsActivity::class.java)
        }

        startActivity(intent)
        finish()
    }
}