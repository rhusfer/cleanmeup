package com.hackathon.cleanmeup.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.hackathon.cleanmeup.R
import kotlinx.android.synthetic.main.activity_login.*

/**
 * Created on 2019-05-05 - 12:49
 * @author Rhusfer John C. Cuezon
 */
class LoginActivity : AppCompatActivity() {
    private lateinit var firebaseAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        firebaseAuth = FirebaseAuth.getInstance()

        buttonLogin.setOnClickListener {
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()

            login(email, password)
        }
    }

    private fun login(email: String, password: String) {
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener {
            if (it.isSuccessful) {
                startActivity(Intent(this, MapsActivity::class.java))
                finish()
            } else {
                AlertDialog.Builder(this)
                    .setTitle("Login")
                    .setMessage("Email or password invalid")
                    .show()
            }
        }
    }
}