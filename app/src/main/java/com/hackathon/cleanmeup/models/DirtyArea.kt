package com.hackathon.cleanmeup.models

import com.google.android.gms.maps.model.LatLng

/**
 * Created on 2019-05-04 - 17:30
 * @author Rhusfer John C. Cuezon
 */
class DirtyArea(
    var name: String,
    var latlng: LatLng,
    var rating: Int,
    var description: String
) {
    var tags: String = "tags"
    var area: Int = 0
    var areaType: String = ""
    var wasteType: String = ""
}